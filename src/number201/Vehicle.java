package number201;

public abstract class Vehicle {
	private String type;
	
	// overloading constructor
	public Vehicle(){
		
	}
	public Vehicle(String type){
		this.type = type;
	}		
	public abstract String typeCar();
	
	public String toString(){
		return type;
	}
}


