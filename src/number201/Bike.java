package number201;

public class Bike extends Vehicle {
	
	public Bike(){

	}
	
	public Bike (String brand){
		super(brand);		
	}
	
	@Override
	public String typeCar(){
		return "sixTires";
	}
	
	public String typeCar(String a){
		return a;
	}
}
