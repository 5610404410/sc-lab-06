package number301;

public class Student {
	public double score = 50;
	private double mathScore = 20;
	protected double engScore = 40;
	
	public double getGrade(double score){
		double midtermExam = 15;
		double finalExam = 20;
		score = midtermExam + finalExam;
		return score;
	}
	public double getScoreMath(double mathScore){
		double mathScoreMidterm = 25;
		double mathScoreFinal = 25;
		mathScore = mathScoreFinal + mathScoreMidterm;
		return mathScore;
	}
	public double getScoreEng(double engScore){
		double engScoreMidterm = 30;
		double engScoreFinal = 30;
		engScore = engScoreFinal +engScoreMidterm;
		return engScore;
	}
}
