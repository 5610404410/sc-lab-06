package number301;

import number302.StudentPP;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student s1 = new Student();
		System.out.println(s1.score);
		System.out.println(s1.getGrade(30));
		
//		System.out.println(s1.mathScore); //ไม่สามารถเรียกได้เนื่องจาก เป็น private จะเรียกค่าได้ต้องทำการ return ค่าออกมา
		System.out.println(s1.getScoreMath(40));
		
		System.out.println(s1.engScore);
		System.out.println(s1.getScoreEng(50));
		
		StudentPP s2 = new StudentPP();
		System.out.println(s2.score);
		
//		System.out.println(s2.mathScore); //ไม่สามารถเรียกได้เนื่องจาก เป็น private จะเรียกค่าได้ต้องทำการ return ค่าออกมา
	
//		System.out.println(s2.engScore); //ไม่สามารถเรียกได้เพราะ อยู่คนละ package
	}

}
