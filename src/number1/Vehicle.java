package number1;

public class Vehicle {
	private String type;
	
	// default constructor
//	public Vehicle(){		
//		
//	}
	
	// overloading constructor
	public Vehicle(String type){
		this.type = type;		
	}	
	
	public String typeCar(){
		return type;
	}
}
