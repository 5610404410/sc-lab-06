package number1;

public class Truck extends Vehicle {
	// default constructor
//	public Truck(){
//		
//	}

	// overloading constructor
	public Truck(String kind){
		super(kind);
	}
	
	@Override
	public String typeCar(){
		return "twelveTires";
	}
}
